const { formatRecordsMock } = require('./mocks/students.mock');
const { formatRecords } = require('./students.utils');
const { rejects, deepStrictEqual } = require('assert');

(async() => {
  {
    const formatedRecords = formatRecordsMock.map(({input, expected}) => {
      const formated = formatRecords(input);

      deepStrictEqual(formated, expected);

      return formated;
    });

    console.log(formatedRecords);

  }
})();