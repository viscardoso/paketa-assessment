const formatRecords = (studentsRecordDetails) => studentsRecordDetails
     .map(student => JSON.parse(student))
     .filter(student => student.graduating && student.studentId)
     .reduce((prevVal, item) => prevVal ?  `${prevVal}, ${item.name}` : item.name, '');

module.exports = { formatRecords };