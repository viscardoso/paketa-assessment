const formatRecordsMock = [
  {
    input: [
      '{"studentId": 7530, "name": "Robert Lee", "graduating": true}',
      '{"name": "Carlin B.", "graduating": true}',
      '{"studentId": 6679, "name": "Jacqui Ostermann", "graduating": true}'
    ],
    expected: 'Robert Lee, Jacqui Ostermann'
  },
  {
    input: [
      '{"studentId": 8357, "name": "Abebe Teka", "graduating": true}',
      '{"studentId": 2350, "name": "Matt Deamon", "graduating": true}',
      '{"studentId": 4440, "name": "Jake J.", "graduating": true}'
    ],
    expected: 'Abebe Teka, Matt Deamon, Jake J.'
  },
  {
    input: [
      '{"studentId": 8540, "name": "Martin G.", "graduating": false}',
      '{"studentId": 3290, "name": "Adam West", "graduating": true}',
      '{"studentId": 3377, "name": "Bekalu Tesema", "graduating": true}'
    ],
    expected: 'Adam West, Bekalu Tesema'
  },
  {
    input: [
      '{"name": "Christopher Nolan", "graduating": true}',
      '{"studentId": 5783, "name": "Martina G.", "graduating": true}',
      '{"studentId": 21, "name": "Whitney brad", "graduating": true}',
      '{"studentId": 2130, "name": "bteshome", "graduating": true}'
    ],
    expected: 'Martina G., Whitney brad, bteshome'
  },
  {
    input: [
      '{"studentId": 193, "name": "Melissa Zerhau", "graduating": true}',
      '{"studentId": 3001, "name": "Aexander Hamilton", "graduating": true}'
    ],
    expected: 'Melissa Zerhau, Aexander Hamilton'
  },
  {
    input: [
      '{"studentId": 905, "name": "alex oc", "graduating": true}',
      '{"studentId": 980, "name": "brook syoum.", "graduating": true}'
  ],
    expected: 'alex oc, brook syoum.'
  }
];

module.exports = { formatRecordsMock };